#include "mainwindow.h"

#include <QApplication>
#include <iostream>
#include <cstdlib>
#include <math.h>

using namespace std;

void devineNombre(){
    cout << "Quel est mon nombre ? " << endl;
    int nombre = rand() % 1000 ;
    int nb = 0;
    int i =0;

    while (nb != nombre) {
       cin >> nb;
       if (nb > nombre) {
           cout << "Le nombre à trouver est plus petit " << endl;
       }
       else if (nb < nombre) {
           cout << "Le nombre à trouver est plus grand " << endl;
       }
       i ++;
    }
    cout << "Félicitations ! Vous avez trouvez le nombre en " << i << " coup(s). " << endl;
}

void devineNombreOrdinateur(){
    cout << "Quel est mon nombre ? " << endl;
    int nombre;
    cin >> nombre;

    int min = 0;
    int max =1000;
    int nbPropose = -1;

    while (nbPropose != nombre) {
        nbPropose = rint((min+max) /2);
        cout << nbPropose << endl;
        if (nbPropose < nombre){
            min = nbPropose;
            cout << "Le nombre à trouver est plus grand"  << endl;
        }
        else if(nbPropose > nombre){
            max = nbPropose;
            cout << "Le nombre à trouver est plus petit"  << endl;
        }
    }
    cout << "Félicitations ! " << endl;
}

int main()
{
    std::string prenom , nom ;
       std::cout << "Quel est votre nom et votre prénom ? " << std::endl;
       std::cin >> nom >> prenom;

       for (int i = 0; i < nom.size(); i++) {
           nom[i] = toupper(nom[i]);
       }

       prenom[0] = toupper(prenom[0]);
       for (int i = 1; i < prenom.size(); i++) {
           prenom[i] = tolower(prenom[i]);
       }

       std::cout << "Bonjour " + prenom + " " + nom + " !" << std::endl;

    //Nombre à trouver par l'utilisateur
    devineNombre();

    //Nombre à trouver par l'ordinateur selon la saisie d'un utilisateur
    devineNombreOrdinateur();

    return 0;
}

