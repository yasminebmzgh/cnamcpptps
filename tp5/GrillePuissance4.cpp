#include "GrillePuissance4.h"

/* CONSTRUCTOR(S) */
GrillePuissance4::GrillePuissance4(int Column, int Row, int Depth, const std::string& Token):VectorGrid(Column, Row, Depth,Token) {}

/* METHOD(S) */
bool GrillePuissance4::PlayerVictory(Joueur Player)
{
    return (checkColumn(4,4,Player.getToken()) || checkRow(4,4,Player.getToken()));
}

