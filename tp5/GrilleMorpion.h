//
// Last modification : 10/11/2021
// GrilleMorpion.h - Daughter class from VectorGrid
//

#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H

#include "Private_Library/VectorGrid.h"
#include "Joueur.h"

class GrilleMorpion : public VectorGrid
{
public:
    /* CONSTRUCTOR(S) */
    GrilleMorpion(int Column, int Row, int Depth, const std::string& Token);

    /* METHOD(S) */
    bool checkDiagonal(int RecursiveLength, int RecursivePresence, const std::string& Token) override {
        int TokenAppear = 0;
        int TokenRecursiveLength = 0;

        /*for (int d = 0; d < getGrid().size(); d++) {
            for (int c = getColumn() - 1; c >= 1;) {
                for (int r = 0; r < getGrid()[d][c].size(); r++) {
                    if (getGrid().at(d).at(r).at(c) == Token) {
                        TokenAppear++;
                        TokenRecursiveLength++;
                        if ((RecursiveLength == TokenRecursiveLength || RecursiveLength <= 1) && RecursivePresence <= TokenAppear)
                        {
                            return true;
                        }
                    } else {
                        TokenRecursiveLength = 0;
                    }
                }
            }
        }*/


        TokenAppear = 0;
        TokenRecursiveLength = 0;


        for (int d = 0; d < getGrid().size(); d++) {
            for (int c = getColumn() - 1; c >= 1;) {
                for (int r = 0; r < getGrid()[d][c].size(); r++) {
                    if (getGrid().at(d).at(r).at(c) == Token) {
                        TokenAppear++;
                        TokenRecursiveLength++;
                        if ((RecursiveLength == TokenRecursiveLength || RecursiveLength <= 1) && RecursivePresence <= TokenAppear)
                        {
                            return true;
                        }
                    } else {
                        TokenRecursiveLength = 0;
                    }
                    if (c >= 1)
                    {
                        c--;
                    }
                }
            }
        }

        return false;
    }

    bool PlayerVictory(Joueur Player);

};

#endif // GRILLEMORPION_H
