#ifndef JOUEUR_H
#define JOUEUR_H

#include <iostream>

class Joueur
{
private:
        /* ATTRIBUTES */
    std::string _nickname;
    std::string _token;
public:
        /* CONSTRUCTOR(S) */
    Joueur(std::string Nickname, std::string Token);
    Joueur();

        /* SETTER(S) */
    void setNickname(std::string Nickname);
    void setToken(std::string Token);

        /* GETTER(S) */
    std::string getNickname();
    std::string getToken();
};

#endif // JOUEUR_H
