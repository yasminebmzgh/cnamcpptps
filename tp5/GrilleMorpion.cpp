//
// Last modification : 10/11/2021
// GrilleMorpion.cpp - Daughter class from VectorGrid
//

#include <iostream>
#include "GrilleMorpion.h"

    /* CONSTRUCTOR(S) */
GrilleMorpion::GrilleMorpion(int Column, int Row, int Depth, const std::string& Token) : VectorGrid(Column, Row, Depth, Token) {}

    /* METHOD(S) */
bool GrilleMorpion::PlayerVictory(Joueur Player)
{
    return (checkColumn(3,3,Player.getToken()) || checkRow(3,3,Player.getToken()) || checkDiagonal(3,3,Player.getToken()));
}
