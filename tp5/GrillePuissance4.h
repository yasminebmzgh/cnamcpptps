//
// Last modification : 10/11/2021
// GrillePuissance4.h - Daughter class from VectorGrid
//

#ifndef GRILLEPUISSANCE4_H
#define GRILLEPUISSANCE4_H

#include "Private_Library/VectorGrid.h"
#include "Joueur.h"

class GrillePuissance4 : public VectorGrid
{
public:
    /* CONSTRUCTOR(S) */
    GrillePuissance4(int Column, int Row, int Depth, const std::string& Token);

    /* METHOD(S) */
    bool checkDiagonal(int RecursiveLength, int RecursivePresence, const std::string& Token) override {
        return false;
    }
    bool PlayerVictory(Joueur Player);

};

#endif // GRILLEPUISSANCE4_H
