//
// Created by Brice - DevOps on 23/11/2021.
//

#ifndef TP5_GRILLEOTHELLO_H
#define TP5_GRILLEOTHELLO_H

#include "Private_Library/VectorGrid.h"
#include "Joueur.h"

class GrilleOthello : public VectorGrid
{
public:
    /* CONSTRUCTOR(S) */
    GrilleOthello(int Column, int Row, int Depth, const std::string& Token);

    /* METHOD(S) */
    bool checkDiagonal(int RecursiveLength, int RecursivePresence, const std::string& Token) override {
        return false;
    }
    bool AroundPosition(GrilleOthello Grid, Joueur Player, int X, int Y);
    bool PlayerVictory(Joueur Player);

};


#endif //TP5_GRILLEOTHELLO_H
