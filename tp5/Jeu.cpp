#include "Jeu.h"

/* CONSTRUCTOR(S) */
Jeu::Jeu() {};

/* METHOD(S) */
void Jeu::launchMorpion(){
        // Initialisation de la grille
    GrilleMorpion Grid(3,3,1," ");

        // Initialisation des deux joueurs
    std::string nickname;

    std::cout << std::endl << "Veuillez saisir le pseudonyme du 1re joueur : "; std::cin >> nickname;
    Joueur playerOne = Joueur(nickname, "X");

    std::cout << std::endl << "Veuillez saisir le pseudonyme du 2de joueur : "; std::cin >> nickname;
    Joueur playerTwo = Joueur(nickname, "O");

        // Variable(s) de partie
    bool isRunning = true;
    bool whoPlay = true;
    int x, y;

        // Début de partie
    while(isRunning)
    {
            // Variable de jeu
        Joueur player;
        std::string choose;

            // Nettoyage de la console et affichage de la grille
        Grid.showGrid();

            // Gestion de l'alternance des joueurs et saisie des coordonnées de jeu.
        if (whoPlay)
        {
            player = Joueur(playerOne);
        } else {
            player = Joueur(playerTwo);
        }

        whoPlay = !whoPlay;

        std::cout << std::endl << player.getNickname() << " a vous de jouer ! ";
        std::cout << std::endl << "Saissiez X | [1-3] : "; std::cin >> y;
        std::cout << std::endl << "Saissiez Y | [1-3] : "; std::cin >> x;

            // Vérification de la possibilité de poser un jeton et relance.
        bool canContinue = false;

        while (!canContinue)
        {
            if (x <= Grid.getRow() && x > 0 && y <= Grid.getColumn() && y > 0)
            {
                if (Grid.getGridValue(x, y, 1) == " ")
                {
                    Grid.setGridValue(x, y, 1, player.getToken());
                    canContinue = true;
                } else {
                    std::cout << std::endl << player.getNickname() << " la case est indisponible, veuillez faire une nouvelle saisie !" << std::endl;
                    system ("PAUSE");
                    Grid.showGrid();
                    std::cout << std::endl << "Saissiez X | [1-3] : "; std::cin >> y;
                    std::cout << std::endl << "Saissiez Y | [1-3] : "; std::cin >> x;
                }
            } else {
                std::cout << std::endl << player.getNickname() << " vos coordonnees sont hors du jeu, veuillez faire une nouvelle saisie !" << std::endl;
                system ("PAUSE");
                Grid.showGrid();
                std::cout << std::endl << "Saissiez X | [1-3] : "; std::cin >> y;
                std::cout << std::endl << "Saissiez Y | [1-3] : "; std::cin >> x;
            }
        }

            // Vérification de la condition de victoire du joueur sinon le niveau de remplissage de la grille.
        if (Grid.PlayerVictory(player))
        {
            Grid.showGrid();
            std::cout << std::endl << "Victoire pour " << player.getNickname() << " felicitation !" << std::endl;
            isRunning = false;
        } else if (!Grid.checkRow(1,1," "))
        {
            std::cout << std::endl << "Match nul.";
            isRunning = false;
        }

            // Si victoire ou nul => Menu
        if (!isRunning)
        {
            system ("PAUSE");

            std::cout << std::endl << "Souhaitez-vous refaire un match ?";
            std::cout << std::endl << "[1] - Oui" << std::endl << "[Autre touche] - Non";
            std::cout << std::endl << "Votre choix : "; std::cin >> choose;

            if (choose == "1")
            {
                Grid.clearGrid(" ");
                isRunning = true;
            } else {
                std::cout << "Au revoir !";
                isRunning = false;
            }
        }
    }
}

void Jeu::launchP4()
{
        // Initialisation de la grille
    GrillePuissance4 Grid(7,4,1," ");

        // Initialisation des deux joueurs
    std::string nickname;

    std::cout << std::endl << "Veuillez saisir le pseudonyme du 1re joueur : "; std::cin >> nickname;
    Joueur playerOne = Joueur(nickname, "X");

    std::cout << std::endl << "Veuillez saisir le pseudonyme du 2de joueur : "; std::cin >> nickname;
    Joueur playerTwo = Joueur(nickname, "O");

        // Variable(s) de partie
    bool isRunning = true;
    bool whoPlay = true;
    int y;

        // Début de partie
    while (isRunning)
    {
            // Variable de jeu
        Joueur player;
        std::string choose;

            // Nettoyage de la console et affichage de la grille
        Grid.showGrid();

            // Gestion de l'alternance des joueurs et saisie des coordonnées de jeu.
        if (whoPlay)
        {
            player = Joueur(playerOne);
        } else {
            player = Joueur(playerTwo);
        }

        whoPlay = !whoPlay;

        std::cout << std::endl << player.getNickname() << " a vous de jouer ! ";
        std::cout << std::endl << "Saissiez X | [1-7] : "; std::cin >> y;

            // Vérification de la possibilité de poser un jeton et relance.
        bool canContinue = false;

        while (!canContinue)
        {
            if (y <= Grid.getRow() && y > 0)
            {
                for (int d = 0; d < Grid.getDepth(); d++) {
                    for (int r = 0; r < Grid.getColumn(); r++) {
                        if (Grid.getGrid().at(d).at(r).at(y - 1) == " " && !canContinue) {
                            Grid.setGridValue(r+1,y,1, player.getToken());
                            canContinue = true;
                            break;
                        }
                    }
                }
                if (!canContinue)
                {
                    std::cout << std::endl << player.getNickname() << " la colonne est indisponible, veuillez faire une nouvelle saisie !" << std::endl;
                    system ("PAUSE");
                    Grid.showGrid();
                    std::cout << std::endl << "Saissiez X | [1-7] : "; std::cin >> y;
                }
            } else {
                std::cout << std::endl << player.getNickname() << " vos coordonnees sont hors du jeu, veuillez faire une nouvelle saisie !" << std::endl;
                system ("PAUSE");
                Grid.showGrid();
                std::cout << std::endl << "Saissiez X | [1-7] : "; std::cin >> y;
            }
        }

            // Vérification de la condition de victoire du joueur sinon le niveau de remplissage de la grille.
        if (Grid.PlayerVictory(player))
        {
            Grid.showGrid();
            std::cout << std::endl << "Victoire pour " << player.getNickname() << " felicitation !" << std::endl;
            isRunning = false;
        } else if (!Grid.checkRow(1,1," "))
        {
            std::cout << std::endl << "Match nul.";
            isRunning = false;
        }

            // Si victoire ou nul => Menu
        if (!isRunning)
        {
            system ("PAUSE");

            std::cout << std::endl << "Souhaitez-vous refaire un match ?";
            std::cout << std::endl << "[1] - Oui" << std::endl << "[Autre touche] - Non";
            std::cout << std::endl << "Votre choix : "; std::cin >> choose;

            if (choose == "1")
            {
                Grid.clearGrid(" ");
                isRunning = true;
            } else {
                std::cout << "Au revoir !";
                isRunning = false;
            }
        }
    }
}

void Jeu::launchOthello()
{
        // Initialisation de la grille
    GrilleOthello Grid(8,8,1," ");
    Grid.setGridValue(4,4,1,"X");
    Grid.setGridValue(5,5,1,"X");
    Grid.setGridValue(4,5,1,"O");
    Grid.setGridValue(5,4,1,"O");

        // Initialisation des deux joueurs
    std::string nickname;

    std::cout << std::endl << "Veuillez saisir le pseudonyme du 1re joueur : "; std::cin >> nickname;
    Joueur playerOne = Joueur(nickname, "X");

    std::cout << std::endl << "Veuillez saisir le pseudonyme du 2de joueur : "; std::cin >> nickname;
    Joueur playerTwo = Joueur(nickname, "O");

    // Variable(s) de partie
    bool isRunning = true;
    bool whoPlay = true;
    int x, y;

    // Début de partie
    while(isRunning) {
        // Variable de jeu
        Joueur player;
        std::string choose;

        // Nettoyage de la console et affichage de la grille
        Grid.showGrid();

        // Gestion de l'alternance des joueurs et saisie des coordonnées de jeu.
        if (whoPlay) {
            player = Joueur(playerOne);
        } else {
            player = Joueur(playerTwo);
        }

        whoPlay = !whoPlay;

        std::cout << std::endl << player.getNickname() << " a vous de jouer ! ";
        std::cout << std::endl << "Saissiez X | [1-8] : ";
        std::cin >> y;
        std::cout << std::endl << "Saissiez Y | [1-8] : ";
        std::cin >> x;

        // Vérification de la possibilité de poser un jeton et relance.
        bool canContinue = false;

        while (!canContinue) {
            if (x <= Grid.getRow() && x > 0 && y <= Grid.getColumn() && y > 0) {
                if (Grid.AroundPosition(Grid,player, y, x))
                {
                    Grid.setGridValue(x, y, 1, player.getToken());
                    canContinue = true;
                } else {
                    std::cout << std::endl << player.getNickname()
                              << " la case est indisponible, veuillez faire une nouvelle saisie !" << std::endl;
                    system("PAUSE");
                    Grid.showGrid();
                    std::cout << std::endl << "Saissiez X | [1-8] : ";
                    std::cin >> y;
                    std::cout << std::endl << "Saissiez Y | [1-8] : ";
                    std::cin >> x;
                }
            } else {
                std::cout << std::endl << player.getNickname()
                          << " vos coordonnees sont hors du jeu, veuillez faire une nouvelle saisie !" << std::endl;
                system("PAUSE");
                Grid.showGrid();
                std::cout << std::endl << "Saissiez X | [1-8] : ";
                std::cin >> y;
                std::cout << std::endl << "Saissiez Y | [1-8] : ";
                std::cin >> x;
            }
        }

        // Vérification de la condition de victoire du joueur sinon le niveau de remplissage de la grille.
        /*if (Grid.PlayerVictory(player)) {
            Grid.showGrid();
            std::cout << std::endl << "Victoire pour " << player.getNickname() << " felicitation !" << std::endl;
            isRunning = false;
        } else */if (!Grid.checkRow(1, 1, " ")) {
            std::cout << std::endl << "Match nul.";
            isRunning = false;
        }

        // Si victoire ou nul => Menu
        if (!isRunning) {
            system("PAUSE");

            std::cout << std::endl << "Souhaitez-vous refaire un match ?";
            std::cout << std::endl << "[1] - Oui" << std::endl << "[Autre touche] - Non";
            std::cout << std::endl << "Votre choix : ";
            std::cin >> choose;

            if (choose == "1") {
                Grid.clearGrid(" ");
                isRunning = true;
            } else {
                std::cout << "Au revoir !";
                isRunning = false;
            }
        }
    }
}
