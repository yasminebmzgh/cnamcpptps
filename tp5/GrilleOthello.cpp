//
// Created by Brice - DevOps on 23/11/2021.
//

#include <iostream>
#include "GrilleOthello.h"

    /* CONSTRUCTOR(S) */
GrilleOthello::GrilleOthello(int Column, int Row, int Depth, const std::string& Token) : VectorGrid(Column, Row, Depth, Token) {}

    /* METHOD(S) */
bool GrilleOthello::AroundPosition(GrilleOthello Grid, Joueur Player, int X, int Y)
{
    for (int y = Y - 1; y <= Y + 1; y++)
    {
        for (int x = X - 1; x <= X + 1; x++)
        {
            if(x >= 0 && y >= 0 && x < 8 && y < 8 && Grid.getGridValue(x,y,1) == " " && Grid.getGridValue(x,y,1) == Player.getToken())
            {
                return true;
            }
        }
    }

    return false;
}

bool GrilleOthello::PlayerVictory(Joueur Player)
{
    return (checkColumn(3,3,Player.getToken()) || checkRow(3,3,Player.getToken()) || checkDiagonal(3,3,Player.getToken()));
}