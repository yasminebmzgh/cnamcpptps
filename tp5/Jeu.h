#ifndef JEU_H
#define JEU_H

#include "GrillePuissance4.h"
#include "GrilleMorpion.h"
#include "GrilleOthello.h"

class Jeu
{
public:
    /* CONSTRUCTOR(S) */
    Jeu();

    /* METHOD(S) */
    void launchMorpion();
    void launchP4();
    void launchOthello();
};

#endif // JEU_H
