#include <iostream>

#include "Jeu.h"
#include "Private_Library/VectorGrid.h"

using namespace std;

int main()
{
    Jeu Game;
    int gameChoose;

    std::cout << "Veuillez choisir un jeu :";
    std::cout << std::endl << "[0] - Jeu du morpion" << std::endl << "[1] - Jeu du puissance 4" << std::endl << "[2] - Jeu d'Othello";
    std::cout << std::endl << "Votre choix : "; std::cin >> gameChoose;

    if (gameChoose == 0)
        Game.launchMorpion();
    else if (gameChoose == 1)
        Game.launchP4();
    else if (gameChoose == 2)
        Game.launchOthello();

    return 0;
}
