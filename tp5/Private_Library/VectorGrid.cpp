//
// Created by Brice - DevOps on 27/10/2021.
//

#include "VectorGrid.h"

/* CONSTRUCTORS */

VectorGrid::VectorGrid(int Row, int Column, int Depth, const std::string& Token) {
    _column = Column;
    _row = Row;
    _depth = Depth;

    for (int d = 0; d < _depth; d++) {
        std::vector<std::vector<std::string>> Xolumn;
        for (int c = 0; c < _column; c++) {
            std::vector<std::string> Xow;
            for (int r = 0; r < _row; r++) {
                Xow.push_back(Token);
            }
            Xolumn.push_back(Xow);
        }
        _grid.push_back(Xolumn);
    }
}

/* SETTER */

void VectorGrid::setGridValue(int Row, int Column, int Depth, std::string Token) {
    _grid.at(Depth - 1).at(Row - 1).at(Column - 1) = std::move(Token);
}

/* GETTERS */

std::string VectorGrid::getGridValue(int Row, int Column, int Depth)
{
    return  _grid.at(Depth - 1).at(Row - 1).at(Column - 1);
}

int VectorGrid::getColumn() const {
    return _column;
}

int VectorGrid::getRow() const {
    return _row;
}

int VectorGrid::getDepth() const {
    return _depth;
}

std::vector<std::vector<std::vector<std::string>>> VectorGrid::getGrid() {
    return _grid;
}

/* METHODS */

bool VectorGrid::checkColumn(int RecursiveLength, int RecursivePresence, const std::string& Token) {

    int TokenAppear = 0;
    int TokenRecursiveLength = 0;

    for (int d = 0; d < _grid.size(); d++) { // 1
        for(int c = 0; c < getColumn(); c++) {
            for(int r = 0; r < _grid[d].size(); r++) { // 4
                if (_grid.at(d).at(r).at(c) == Token) {
                    TokenAppear++;
                    TokenRecursiveLength++;
                    if ((RecursiveLength == TokenRecursiveLength || RecursiveLength <= 1) && RecursivePresence <= TokenAppear)
                    {
                        return true;
                    }
                } else {
                    TokenRecursiveLength = 0;
                }
            }
            TokenRecursiveLength = 0;
        }
    }

    return false;
}

bool VectorGrid::checkRow(int RecursiveLength, int RecursivePresence, const std::string& Token) {

    int TokenAppear = 0;
    int TokenRecursiveLength = 0;

    for (int d = 0; d < _grid.size(); d++) {
        for (int c = 0; c < _grid[d].size(); c++) {
            for (int r = 0; r < _grid[d][c].size(); r++) {
                if (_grid.at(d).at(c).at(r) == Token) {
                    TokenAppear++;
                    TokenRecursiveLength++;
                    if ((RecursiveLength == TokenRecursiveLength || RecursiveLength <= 1) && RecursivePresence <= TokenAppear)
                    {
                        return true;
                    }
                } else {
                    TokenRecursiveLength = 0;
                }
            }
        }
    }

    return false;
}

void VectorGrid::clearGrid(const std::string& Token) {
    for (int d = 0; d < _grid.size(); d++) {
        for (int c = 0; c < _grid[d].size(); c++) {
            for (int r = 0; r < _grid[d][c].size(); r++) {
                _grid.at(d).at(c).at(r) = Token;
            }
        }
    }
}

void VectorGrid::showGrid() {
    for (int d = 0; d < _depth; d++) {
        std::cout << std::endl;
        for (int c = 0; c < _column; c++) {
            std::cout<< std::endl << "| ";
            for (int r = 0; r < _row; r++) {
                std::cout << _grid[d][c][r] << " | ";
            }
        }
    }
}