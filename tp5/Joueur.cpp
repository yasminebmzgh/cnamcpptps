#include "Joueur.h"

    /* CONSTRUCTOR(S) */
Joueur::Joueur() {};

Joueur::Joueur(std::string Nickname, std::string Token)
{
    _nickname = std::move(Nickname);
    _token = std::move(Token);
}

    /* SETTER(S) */
void Joueur::setNickname(std::string Nickname)
{
    _nickname = std::move(Nickname);
}

void Joueur::setToken(std::string Token)
{
    _token = std::move(Token);
}

    /* GETTER(S) */
std::string Joueur::getNickname()
{
    return _nickname;
}

std::string Joueur::getToken()
{
    return _token;
}

