#include "diagonale.h"

Diagonale::Diagonale(vector<Case*> c)
{
    this->m_cases = c;
}
bool Diagonale::isComplete(Joueur j){
    for(unsigned i = 0; i < this->m_cases.size(); i++){
        if(!this->m_cases[i]->occupeParJoueur(j)){
            return false;
        }
    }
    return true;
}
