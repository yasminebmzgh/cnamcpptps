QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    case.cpp \
    diagonale.cpp \
    grille.cpp \
    grillemorpion.cpp \
    grillepuissance4.cpp \
    jeu.cpp \
    joueur.cpp \
    main.cpp

HEADERS += \
    case.h \
    diagonale.h \
    grille.h \
    grillemorpion.h \
    grillepuissance4.h \
    jeu.h \
    joueur.h

FORMS +=

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
