#ifndef CASE_H
#define CASE_H

#include "joueur.h"
using namespace std;

class Case
{
public:
    Case(int m_x,int m_y);

    bool isVide();
    int getX() const;
    int getY() const;
    void setJoueur(Joueur j);
    bool occupeParJoueur(Joueur j);
    Joueur getJoueur();

private:
    int m_x;
    int m_y;
    Joueur m_joueur = *(new Joueur(0));
};

#endif // CASE_H
