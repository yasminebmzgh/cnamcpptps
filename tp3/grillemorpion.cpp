#include "grillemorpion.h"

GrilleMorpion::GrilleMorpion()
{
    this->m_ligne = this->m_colonne = 3;
    for(int i=0; i<this->m_ligne; i++){
        for(int j=0; j<this->m_colonne; j++){
            this->m_grille.push_back(new Case(i,j));
        }
    }
    vector <Case*> casesDiag1 = vector<Case*>();
    for(int i= 0; i < this->m_colonne; i++){
        casesDiag1.push_back(GetCase(i,i));
    }
    vector<Case*> casesDiag2 = vector<Case*>();
    for(int i =2; i >=0; i--){
        for(int j =0; j <=2; j++){
            casesDiag2.push_back(GetCase(j,i));
        }
    }

    this->m_diagonales.push_back(*(new Diagonale(casesDiag1)));
    this->m_diagonales.push_back(*(new Diagonale(casesDiag2)));
}

bool GrilleMorpion::LigneComplete(Joueur joueur, int ligne)
{
    for(int i=0; i< this->m_colonne; i++){
        Case* case1 = GetCase(ligne,i);
        if(!case1->occupeParJoueur(joueur)){
            return false;
        }
    }
    return true;
}
bool GrilleMorpion::VictoireJoueur(Joueur joueur)
{
    for(int i =0; i < this->m_ligne; i++){
        if(LigneComplete(joueur, i)){
            return true;
        }
    }
    for(int j=0; j < this->m_colonne; j++){
        if(ColonneComplete(joueur, j)){
            return true;
        }
    }
    for(unsigned k = 0; k < this->m_diagonales.size(); k++){
        if(DiagonaleComplete(joueur,this->m_diagonales[k])){
            return true;
        }
    }
    return false;
}
Case* GrilleMorpion::getCaseOfColonne(int colonne){
    return NULL;
}
