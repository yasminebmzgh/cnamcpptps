#include "grillepuissance4.h"

GrillePuissance4::GrillePuissance4()
{
    this->m_ligne = 4;
    this ->m_colonne = 7;
    for(int i=0; i<this->m_ligne; i++){
        for(int j=0; j<this->m_colonne; j++){
            this->m_grille.push_back(new Case(i,j));
        }
    }

    //Initialisation des diagonales
    for(int compteur = 0; compteur < 4; compteur++){
        vector<Case*> casesDiag = vector<Case*>();
        for(int i = 0; i <4; i++){
            casesDiag.push_back(new Case(3-i,compteur+i));
        }
        this->m_diagonales.push_back(*(new Diagonale(casesDiag)));
    }
    for(int compteur= 0; compteur < 4; compteur++){
        vector<Case*> casesDiag = vector<Case*>();
        for(int i = 0; i < 4; i++){
            casesDiag.push_back(new Case(i,compteur+i));
        }
        this->m_diagonales.push_back(*(new Diagonale(casesDiag)));
    }

}

bool GrillePuissance4::LigneComplete(Joueur joueur, int ligne)
{
    int i =0;
    bool result  = false;
    while(i < this->m_colonne){
        Case* case1 = GetCase(ligne, i);
        if(case1->occupeParJoueur(joueur) && i != this->m_colonne -1){
            int compteur = 0;
            i++;
            while(GetCase(ligne,i)->occupeParJoueur(joueur)){
                compteur++;
                i++;
            }
            if(compteur > 3){
                result = true;
            }
        }else{
            i++;
        }
    }
    return result;
}

bool GrillePuissance4::VictoireJoueur(Joueur joueur)
{
    for(int i =0; i < this->m_ligne; i++){
        if(LigneComplete(joueur, i)){
            return true;
        }
    }
    for(int j=0; j < this->m_colonne; j++){
        if(ColonneComplete(joueur, j)){
            return true;
        }
    }
    for(unsigned k = 0; k < this->m_diagonales.size(); k++){
        if(DiagonaleComplete(joueur,this->m_diagonales[k])){
            return true;
        }
    }
    return false;
}

Case* GrillePuissance4::getCaseOfColonne(int colonne){
    if(colonne < this->m_colonne){
        Case* minCase = new Case(90000,90000);
        for(unsigned i=0; i < this->m_grille.size(); i++){
           Case* case1 = this->m_grille[i];
           if(case1->getY() == colonne){
               if(case1->getX() < minCase->getX() && case1->isVide()){
                   minCase = case1;
               }
           }
        }
        return minCase;
    }
    return NULL;
}
