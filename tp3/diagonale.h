#ifndef DIAGONALE_H
#define DIAGONALE_H

#include <vector>

#include "case.h"

class Diagonale
{
public:
    Diagonale(vector<Case*> c);

    bool isComplete(Joueur j);
private:
    vector<Case*> m_cases = vector<Case*>();
};

#endif // DIAGONALE_H
