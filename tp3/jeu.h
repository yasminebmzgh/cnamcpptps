#ifndef JEU_H
#define JEU_H

#include "grille.h"
#include "grillepuissance4.h"
#include "grillemorpion.h"

class Jeu
{
public:
    Jeu(int choixJeu);
private:
    Grille* grille;
    void lancerMorpion();
    void lancerPuissance4();
};

#endif // JEU_H
