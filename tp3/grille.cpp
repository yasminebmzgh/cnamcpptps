#include "grille.h"
#include <vector>
using namespace std;
void Grille::DeposerJeton(Joueur joueur, Case* c)
{
    if(c->isVide()){
        c->setJoueur(joueur);
    }
}

void Grille::AfficherGrille()
{
    for(int i = 0; i < this->m_ligne; i++){
        for(int j = 0; j < this->m_colonne; j++){
            Case* case1 = GetCase(i,j);
            cout << case1->getJoueur().getId();
        }
        cout << endl;
    }
}

Case* Grille::GetCase(int x, int y) const{
    if(x < this->m_ligne && y < this->m_colonne){
        for(unsigned i=0; i < this->m_grille.size(); i++){
           Case* case1 = this->m_grille[i];
           if(case1->getX() == x && case1->getY() == y){
               return case1;
           }
        }
    }
    return NULL;
}

bool Grille::isPleine(){
    for(unsigned i =0; i< this->m_grille.size(); i++){
        if(this->m_grille[i]->isVide()){
            return false;
        }
    }
    return true;
}
int Grille::getNbLigne(){
    return this->m_ligne;
}
int Grille::getNbColonne(){
    return this->m_colonne;
}
bool Grille::DiagonaleComplete(Joueur joueur, Diagonale d) const
{
    return d.isComplete(joueur);
}
bool Grille::ColonneComplete(Joueur joueur, int colonne) const
{
    for(int i=0; i < this->m_ligne; i++){
        Case* case1 = GetCase(i,colonne);
        if(!case1->occupeParJoueur(joueur)){
            return false;
        }
    }
    return true;
}
