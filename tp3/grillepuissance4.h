#ifndef GRILLEPUISSANCE4_H
#define GRILLEPUISSANCE4_H

#include <iostream>
#include <vector>

#include "grille.h"
class GrillePuissance4:public Grille
{
public:
    // Constructeurs
    GrillePuissance4();

    //méthodes
    bool LigneComplete(Joueur joueur, int ligne) override;
    bool VictoireJoueur(Joueur joueur) override;
    Case* getCaseOfColonne(int colonne) override;
};

#endif // GRILLEPUISSANCE4_H
