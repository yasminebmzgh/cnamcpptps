# Module : Développement C++
# TP 4

[Dépôt Yasmine](https://gitlab.com/yasminebmzgh/cnamcpptps/-/tree/main/tp3)

[Dépôt Brice](https://github.com/thepiratebot/a1_cpp/tree/master/TP3)


## Principe S : 

- La classe Jeu.cpp de Brice a deux responsabilités : gérer les jeux mais aussi les joueurs dans chacun des jeux respectifs. Cette classe ne respecte donc pas le principe S.
```cpp
class Jeu {
public:
    /* CONSTRUCTORS */
    Jeu();

    /* METHODS */
    void newMorpionGame();
    void newP4Game();
};
```

- Alors que Yasmine, distingue deux classes, une classe jeu.cpp qui se charge de gérer les jeux et une classe joueur.cpp qui s'occupe des joueurs. 
De cette façon le principe S est mieux respecté.
```cpp
class Jeu
{
public:
    Jeu(int choixJeu);
private:
    Grille* grille;
    void lancerMorpion();
    void lancerPuissance4();
};

```
```cpp
class Joueur
{
public:
    Joueur(int id);

    int getId();
private:
    int m_id;
};
```

## Principe O : 

- La classe Grid.cpp de Brice respect le principe O, en effet il est possible d'ajouter des fonctionnalités sans que cela pose un problème.
```cpp
class Grid {
private:
    int _height;
    int _width;
    std::string** _array;

public:
    /* CONSTRUCTORS */
    Grid(int Height, int Width);

    /* GETTER */
    int getHeight() const;
    int getWidth() const;
    std::string** getArray();
    std::string getValue(int X, int Y);

    /* SETTER */
    void setValue(int X, int Y, std::string Value);

    /* METHODS */
    //bool checkRow(int RecursiveLength, bool Continue); *Futur fonction*
    //bool checkColumn(int RecursiveLength, bool Continue); *Futur fonction*

    bool isCellEmpty(int X, int Y);
    void clearGrid();
    void showGrid();
};
```

- Yasmine respect le principe O via la classe joueur.cpp, en effet si il faut ajouter des fonctionnalités, cela ne posera pas problème. 
```cpp
class Jeu
{
public:
    Jeu(int choixJeu);
private:
    Grille* grille;
    void lancerMorpion();
    void lancerPuissance4();
};
```



## Principe L : 

- Brice respect le principe L, en effet, la classe fille GrilleMorpion.cpp, peut générer des grilles via l'héritage.
```cpp
class GrilleMorpion : public Grid {
public:
    /* CONSTRUCTORS */
    GrilleMorpion(int Height, int Width);

    /* METHODS */
    bool checkHeight(int Height, std::string Token);
    bool checkWidth(int Width, std::string Token);
    bool checkDiagonal(const std::string& Token);

    bool playerVictory(int Height, int Width, const std::string& Token);
};
```

- Yasmine respect le principe L, en effet, la classe fille grillemorpion.cpp, peut générer des grilles via l'héritage.
```cpp
class GrilleMorpion:public Grille
{
public:
    // Constructeurs
    GrilleMorpion();

    //méthodes
    bool LigneComplete(Joueur joueur, int ligne) override;
    bool VictoireJoueur(Joueur joueur) override;
    Case* getCaseOfColonne(int colonne) override;
};
```

## Principe I : 

- Brice ne respect pas le principe I, en effet il ne dispose pas d'interface.
- Yasmine respect le principe I, en effet, la classe fille grillemorpion.cpp et la calsse grillepuissance4.cpp disposent tout deux de fonction redéfinie, la gestion est correctement faite.
```cpp
class GrillePuissance4:public Grille
{
public:
    // Constructeurs
    GrillePuissance4();

    //méthodes
    bool LigneComplete(Joueur joueur, int ligne) override;
    bool VictoireJoueur(Joueur joueur) override;
    Case* getCaseOfColonne(int colonne) override;
};
```

## Principe D : 

- Brice ne respect pas le principe D, en effet il dispose d'une classe de haut niveau, de classes de bas niveau, d'héritage mais pas d'abstraction et donc pas d'interface.
- Yasmine respect le principe D, en effet, la classe fille grillemorpion.cpp et la calsse grillepuissance4.cpp disposent tout deux de fonction redéfinie, et de fonction hérité de la classe mère grille.cpp.
```cpp
class GrilleMorpion:public Grille
{
public:
    // Constructeurs
    GrilleMorpion();

    //méthodes
    bool LigneComplete(Joueur joueur, int ligne) override;
    bool VictoireJoueur(Joueur joueur) override;
    Case* getCaseOfColonne(int colonne) override;
};
```

## Loi de Déméter : 

- Brice respect la loi de Déméter, en effet aucun objet A fait de pont vers B pour demander à C.
- Yasmine respect la loi de Déméter, en effet aucun objet A fait de pont vers B pour demander à C.

## Tell don't ask : 

- Brice respect le Tell don't ask, en effet aucun objet A intéragit avec B pour obtenir réponse à son propre état.
- Yasmine respect le Tell don't ask, en effet aucun objet A intéragit avec B pour obtenir réponse à son propre état.


