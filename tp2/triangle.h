#ifndef TRIANGLE_H
#define TRIANGLE_H
#include <iostream>
using namespace std;

#include "Point.h"


class Triangle
{
private:
  struct Point a, b, c;

public:
    Triangle();
    Triangle(struct Point a, struct Point b, struct Point c);

     //setters et getters
     void SetA(struct Point a);
     void SetB(struct Point b);
     void SetC(struct Point c);
     struct Point GetA() const;
     struct Point GetB() const;
     struct Point GetC() const;

     double Distance( Point a,  Point b) const;
     double Base() const;
     double Hauteur() const;
     double Surface() const;
     double Longueurs() const;
     bool Isocele() const;
     bool Isrectangle() const;
     bool Isequilateral() const;
     void Afficher() const;

};

#endif // TRIANGLE_H
