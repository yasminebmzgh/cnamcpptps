#include "cercle.h"
#include "Point.h"
#include "math.h"
#include <iostream>

using namespace std;


Cercle::Cercle(struct Point _centre,int _diametre)
{
    centre=_centre;
    diametre=_diametre;
}
void Cercle::setCentre(struct Point _centre){
   centre = _centre;
}
Point Cercle::getCentre() const {
   return centre;
}

void Cercle::setDiametre(int d){
   diametre = d;
}
int Cercle::getDiametre() const {
   return diametre;
}

double Cercle::Perimetre() const {
    return 2*M_PI * (getDiametre()/2);
}

double Cercle::Surface() const {
    return 2*M_PI * (pow(getDiametre(), 2)/2);
}

bool Cercle::Sur_le_cercle(struct Point p) const{

return pow((p._x - centre._x),2) + pow((p._y- centre._y),2) == pow(getDiametre()/2,2);
}

bool Cercle::Dans_le_cercle(struct Point p) const{
    double distance = sqrt( pow(p._x - centre._x, 2) + pow(p._y - centre._y, 2) );
    return distance < (this->getDiametre()/2);

}


void Cercle::Afficher() const
{
    cout << "cercle :" << this->centre._x << this->centre._y << this->diametre << endl;
}

