#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <stdio.h>
#include "Point.h"

class Rectangle {
private:
    double longueur;
    double largeur;
    struct Point coin_sup_gauche;


public:
    // Constructeurs
    Rectangle();
    Rectangle(int longueur, int largeur, Point p);

    //setters et getters
    void setLongeur(double l);
    double getLongeur() const;
    void setLargeur(double l);
    double getLargeur() const;
    void setCoin_sup_gauche(struct Point p);
    struct Point getCoin_sup_gauche() const;

    double Perimetre() const;
    double Surface() const;
    void Afficher() const;
};

#endif /* Rectangle_hpp */

