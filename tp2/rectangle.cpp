#include "rectangle.h"
#include <iostream>
#include "Point.h"

Rectangle::Rectangle() : longueur(0), largeur(0)
{}

Rectangle::Rectangle(int _longueur, int _largeur, Point _p)
{
    longueur=_longueur;
    largeur=_largeur;
    coin_sup_gauche=_p;
}
void Rectangle::setLongeur(double _longueur){
       longueur = _longueur;
   }
   double Rectangle::getLongeur() const{
       return longueur;
   }

   void Rectangle::setLargeur(double _largeur){
       largeur = _largeur;
   }
   double Rectangle::getLargeur() const{
       return largeur;
   }

   void Rectangle::setCoin_sup_gauche(Point _p){
       coin_sup_gauche = _p;
   }
   struct Point Rectangle::getCoin_sup_gauche() const{
       return coin_sup_gauche;
   }

   double Rectangle::Perimetre() const{
       return (largeur + longueur) * 2;
   }

   double Rectangle::Surface() const{
       return largeur * longueur;
   }

void Rectangle::Afficher() const{
   std::cout << "Longueur du rectangle : " << longueur << std::endl;
   std::cout << "Largeur du rectangle : " << largeur << std::endl;
   std::cout << "Coordonnées du coin supérieur gauche : (" << coin_sup_gauche._x << "," << coin_sup_gauche._y << ")" << std::endl;
   std::cout << "Périmètre du rectangle : " << Perimetre() << std::endl;
   std::cout << "Surface du rectangle : " << Surface() << std::endl;
}

