#ifndef CERCLE_H
#define CERCLE_H
#include "Point.h"

class Cercle{
private:
        struct Point centre;
        int diametre;
public:
    // Constructeurs
    Cercle();
    Cercle(Point centre, int diametre);

    //setters et getters
    void setCentre(Point centre);
    Point getCentre() const ;
    void setDiametre(int d);
    int getDiametre() const ;


    double Perimetre() const ;
    double Surface() const ;
    bool Sur_le_cercle(Point p) const;
    bool Dans_le_cercle(Point p) const;
    void Afficher() const;

};

#endif // CERCLE_H
