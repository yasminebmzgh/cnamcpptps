#include "triangle.h"

#include <math.h>

#include "Point.h"
#include "triangle.h"

using namespace std;


Triangle::Triangle()
{

}
Triangle::Triangle( Point a,  Point b,  Point c)
{
    this->a=a;
    this->b=b;
    this->c=c;
}


void Triangle::SetA( Point a)
{
    this->a = a;
}

void Triangle::SetB( Point b)
{
    this->b = b;
}

void Triangle::SetC( Point c)
{
    this->c = c;
}


struct Point Triangle::GetA() const
{
    return this->a;
}

struct Point Triangle::GetB() const
{
    return this->b;
}

struct Point Triangle::GetC() const
{
    return this->c;
}

double Triangle::Distance( struct Point a,  struct Point b) const
{
    double distance;
    distance = sqrt( pow(a._x - b._x, 2) + pow(a._y - b._y, 2) );
    return distance;
}

double Triangle::Base() const
{
    double base;

    double a = this->Distance(this->a,this->b);
    double b = this->Distance(this->b,this->c);
    double c = this->Distance(this->a,this->c);

    if (a < b) {
        base = a;
    }
    else {
        base = b;
    }

    if (base < c) {
        base = c;
    }

    return base;
}

double Triangle::Hauteur() const
{
    return (2*this->Surface()) / this->Base();
}

double Triangle::Surface() const
{
    double a = Distance(this->a, this->b);
    double b = Distance(this->b, this->c);
    double c = Distance(this->a, this->c);
    double p = (a + b + c) /2;
    return sqrt(p* (p-a) * (p-b) * (p-c));
}

double Triangle::Longueurs() const
{
    return Distance(this->a, this->b) + Distance(this->b, this->c) + Distance(this->a, this->c);
}

bool Triangle::Isocele() const
{
    bool iso = false;
    if ((Distance(this->a, this->b) == Distance(this->b, this->c))
            || (Distance(this->b, this->c) == Distance(this->a, this->c))
            || (Distance(this->a, this->b) == Distance(this->a, this->c)))
    {
       iso = true;
    }
    return iso;
}


bool Triangle::Isrectangle() const
{
    bool rec = false;
    if (pow(Distance(this->b, this->c),2) == pow(Distance(this->a, this->b),2) + pow(Distance(this->a, this->c),2))
    {
        rec = true;
    }
    return rec;
}


bool Triangle::Isequilateral() const
{
    bool equi = false;
    if (Distance(this->a, this->b) == Distance(this->b, this->c) == Distance(this->a, this->b))
    {
        equi = true;
    }
    return equi;
}

void Triangle::Afficher() const
{
    cout << "Voici un triangle : " << this->a._x << this->a._y << this->b._x << this->b._y << this->c._x << this->c._y << endl;
}
